window.onload = function () {
	document.body.style.backgroundColor = "#3B3B3B";

	const a = prompt("Entrez un nombre");
	console.log("🚀 ~ file: exo22.js:3 ~ a:", a);
	const b = parseFloat(a);
	console.log("🚀 ~ file: exo22.js:7 ~ b:", b);
	const resultat = typeof b;
	console.log("🚀  ~TYPE OF  resultat:", resultat);
	console.log("🚀 ~ file: exo22.js:5 ~ b:", b);

	if (!isNaN(b)) {
		console.log(b);
		document.body.style.backgroundColor = "green";
		alert("Vous avez bien saisi un nombre");
	} else {
		document.body.style.backgroundColor = "red";

		alert("Vous n'avez pas saisis une valeur convertible en nombre");
	}

	//         if (typeof b === "Nan") {
	//		console.log(b);
	//		document.body.style.backgroundColor = "green";
	//		alert("Vous avez bien saisi un nombre");
	//	} else {
	//		document.body.style.backgroundColor = "red";
	//
	//		alert("Vous n'avez pas saisis une valeur convertible en nombre");
	//	}
};
